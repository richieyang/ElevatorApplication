﻿using System.Collections.Generic;
using ElevatorApplication.Elevator;
using ElevatorApplication.Elevator.ButtonOfFloor;

namespace ElevatorApplication.ElevatorStates
{
    public interface IElevatorState
    {
        Floor CurrentFloor { get; }

        IReadOnlyList<ButtonsOfFloor> ButtonStateInFloors { get; }

        void Run();
    }
}