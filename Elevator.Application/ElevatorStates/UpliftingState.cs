﻿using System;
using System.Collections.Generic;
using ElevatorApplication.Elevator;
using ElevatorApplication.Elevator.ButtonOfFloor;

namespace ElevatorApplication.ElevatorStates
{
    public class UpliftingState : IElevatorState
    {
        private readonly IStatefulMachine _statefulMachine;
        private readonly List<ButtonsOfFloor> _buttonsOfFloors;
        
        public Floor CurrentFloor { get; private set; }

        public IReadOnlyList<ButtonsOfFloor> ButtonStateInFloors
        {
            get { return _buttonsOfFloors.AsReadOnly(); }
        }

        public UpliftingState(IStatefulMachine statefulMachine, List<ButtonsOfFloor> buttonsOfFloors, Floor currentFloor)
        {
            _statefulMachine = statefulMachine;
            _buttonsOfFloors = buttonsOfFloors;
            CurrentFloor = currentFloor;
        }

        public void Run()
        {
            if (!this.IsHigherFloorButtonBePushed())
            {
                var state = new StoppedState(_statefulMachine, _buttonsOfFloors, CurrentFloor);
                _statefulMachine.ChangeState(state);
                Console.WriteLine("elevator is stopped in " + CurrentFloor + " floor");
                return;
            }
            var nextFloor = this.GetNextHigherBePushedFloor();
            for (int i = CurrentFloor.Number + 1; i < nextFloor.Number + 1; i++)
            {
                CurrentFloor = CurrentFloor.GetNextHigherFloor();

                Console.WriteLine("elevator is in " + i + " floor");
            }

            this.ResetButtonsWhenElevatorStillLifting();

            Run();
        }
    }
}