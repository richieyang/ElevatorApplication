﻿using System.Collections.Generic;
using ElevatorApplication.Elevator;
using ElevatorApplication.Elevator.ButtonOfFloor;

namespace ElevatorApplication.ElevatorStates
{
    public class StoppedState : IElevatorState
    {
        private readonly IStatefulMachine _statefulMachine;
        private readonly List<ButtonsOfFloor> _buttonsOfFloors;

        public Floor CurrentFloor { get; private set; }

        public IReadOnlyList<ButtonsOfFloor> ButtonStateInFloors
        {
            get { return _buttonsOfFloors.AsReadOnly(); }
        }

        public StoppedState(IStatefulMachine statefulMachine, List<ButtonsOfFloor> buttonsOfFloors, Floor currentFloor)
        {
            _statefulMachine = statefulMachine;
            _buttonsOfFloors = buttonsOfFloors;
            CurrentFloor = currentFloor;
        }

        public void Run()
        {
            this.ResetButtonsWhenElevatorStillLifting();

            if (this.IsHigherFloorButtonBePushed())
            {
                var state = new UpliftingState(_statefulMachine, _buttonsOfFloors, CurrentFloor);
                _statefulMachine.ChangeState(state);
            }

            if (this.IsLowerFloorButtonBePushed())
            {
                var state = new DecliningState(_statefulMachine, _buttonsOfFloors, CurrentFloor);
                _statefulMachine.ChangeState(state);
            }
        }
    }
}