﻿using System;
using System.Collections.Generic;
using ElevatorApplication.Elevator;
using ElevatorApplication.Elevator.ButtonOfFloor;

namespace ElevatorApplication.ElevatorStates
{
    public class DecliningState : IElevatorState
    {
        private readonly IStatefulMachine _statefulMachine;
        private readonly List<ButtonsOfFloor> _buttonsOfFloors;
        
        public Floor CurrentFloor { get; private set; }

        public IReadOnlyList<ButtonsOfFloor> ButtonStateInFloors
        {
            get { return _buttonsOfFloors; }
        }

        public DecliningState(IStatefulMachine statefulMachine, List<ButtonsOfFloor> buttonsOfFloors, Floor currentFloor)
        {
            _statefulMachine = statefulMachine;
            _buttonsOfFloors = buttonsOfFloors;
            CurrentFloor = currentFloor;
        }


        public void Run()
        {
            if (!this.IsLowerFloorButtonBePushed())
            {
                var state = new StoppedState(_statefulMachine, _buttonsOfFloors, CurrentFloor);
                _statefulMachine.ChangeState(state);
                Console.WriteLine("elevator is stopped in " + CurrentFloor + " floor");
                return;
            }

            var nextFloor = this.GetNextLowerBePushedFloor();
            for (int i = CurrentFloor.Number - 1; i > nextFloor.Number - 1; i--)
            {
                CurrentFloor = CurrentFloor.GetNextLowerFloor();

                Console.WriteLine("elevator is in " + i + " floor");
            }

            this.ResetButtonsWhenElevatorStillDecling();

            Run();
        }
    }
}