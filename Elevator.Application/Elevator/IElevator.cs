﻿using System.Collections.Generic;
using ElevatorApplication.Elevator.ButtonOfFloor;
using ElevatorApplication.ElevatorStates;

namespace ElevatorApplication.Elevator
{
    public interface IElevator
    {
        Floor CurrentFloor { get; }

        Floor FloorHeight { get; }

        IReadOnlyList<ButtonsOfFloor> ButtonStateInFloors { get; }

        void PushUpButton(Floor floor);

        void PushDownButton(Floor floor);

        void PushNumberButton(Floor floor);
    }

    public interface IStatefulMachine
    {
        void ChangeState(IElevatorState newElevatorState);
    }

    public interface ICanStart
    {
        void Start();
    }
}