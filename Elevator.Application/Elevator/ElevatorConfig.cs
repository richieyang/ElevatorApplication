﻿namespace ElevatorApplication.Elevator
{
    public class ElevatorConfig
    {
        public ElevatorConfig()
        {
            InitializedFloor = Floor.Parse(1);
            FloorHeight=Floor.Parse(20);
        }

        public Floor InitializedFloor { get; set; }

        public Floor FloorHeight { get; set; }
    }
}