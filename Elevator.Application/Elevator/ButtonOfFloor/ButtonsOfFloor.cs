namespace ElevatorApplication.Elevator.ButtonOfFloor
{
    public class ButtonsOfFloor
    {
        public ButtonsOfFloor(Floor floor)
        {
            //Validation

            Floor = floor;

            IsUpButtonBePushed = false;
            IsDownButtonBePushed = false;
            IsNumberButtonBePushed = false;
        }

        public Floor Floor { get; private set; }
        public bool IsUpButtonBePushed { get; private set; }
        public bool IsDownButtonBePushed { get; private set; }
        public bool IsNumberButtonBePushed { get; private set; }

        public void PushUpButton()
        {
            IsUpButtonBePushed = true;
        }

        public void PushDownButton()
        {
            IsDownButtonBePushed = true;
        }

        public void PushNumberButton()
        {
            IsNumberButtonBePushed = true;
        }

        public void ResetAllButtons()
        {
            IsUpButtonBePushed = false;
            IsDownButtonBePushed = false;
            IsNumberButtonBePushed = false;
        }

        public void ResetUpwardButtons()
        {
            IsUpButtonBePushed = false;
            IsNumberButtonBePushed = false;
        }

        public void ResetDownwardButtons()
        {
            IsDownButtonBePushed = false;
            IsNumberButtonBePushed = false;
        }
    }
}
