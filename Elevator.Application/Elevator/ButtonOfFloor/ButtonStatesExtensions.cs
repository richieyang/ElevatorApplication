using System.Collections.Generic;
using System.Linq;

namespace ElevatorApplication.Elevator.ButtonOfFloor
{
    public static class ButtonStatesExtensions
    {
        public static ButtonsOfFloor For(this List<ButtonsOfFloor> list, Floor floor)
        {
            return list.Single(x => x.Floor.Equals(floor));
        }

        public static List<ButtonsOfFloor> InitializeElevatorButtonsForAllFloors(this Floor floor)
        {
            return Enumerable.Range(1, floor.Number).Select(x => new ButtonsOfFloor(Floor.Parse(x))).ToList();
        }
    }
}
