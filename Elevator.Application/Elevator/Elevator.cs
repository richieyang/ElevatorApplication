﻿using System.Collections.Generic;
using ElevatorApplication.Elevator.ButtonOfFloor;
using ElevatorApplication.ElevatorStates;

namespace ElevatorApplication.Elevator
{
    public class Elevator : IElevator, IStatefulMachine, ICanStart
    {
        public Floor CurrentFloor
        {
            get { return _elevatorState.CurrentFloor; }
        }

        public Floor FloorHeight
        {
            get { return Floor.FloorHeight; }
        }

        public IReadOnlyList<ButtonsOfFloor> ButtonStateInFloors
        {
            get { return _buttonsOfFloors.AsReadOnly(); }
        }

        private IElevatorState _elevatorState;
        private readonly List<ButtonsOfFloor> _buttonsOfFloors;

        public Elevator(ElevatorConfig config)
        {
            Floor.FloorHeight = config.FloorHeight;

            _buttonsOfFloors = FloorHeight.InitializeElevatorButtonsForAllFloors();
            _elevatorState = new StoppedState(this, _buttonsOfFloors, config.InitializedFloor);
        }

        public void PushUpButton(Floor floor)
        {
            _buttonsOfFloors.For(floor).PushUpButton();
        }

        public void PushDownButton(Floor floor)
        {
            _buttonsOfFloors.For(floor).PushDownButton();
        }

        public void PushNumberButton(Floor floor)
        {
            _buttonsOfFloors.For(floor).PushNumberButton();
        }

        public void Start()
        {
            _elevatorState.Run();
        }

        public void ChangeState(IElevatorState newElevatorState)
        {
            _elevatorState = newElevatorState;
            _elevatorState.Run();
        }
    }
}