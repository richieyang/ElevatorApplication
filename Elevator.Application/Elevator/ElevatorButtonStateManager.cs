﻿using System.Linq;
using ElevatorApplication.ElevatorStates;

namespace ElevatorApplication.Elevator
{
    public static class ElevatorButtonStateManager
    {
        public static bool IsHigherFloorButtonBePushed(this IElevatorState elevatorState)
        {
            // ReSharper disable once ComplexConditionExpression
            var clicked = elevatorState.ButtonStateInFloors
                .Where(x => x.Floor.Number > elevatorState.CurrentFloor.Number)
                .Any(x => x.IsUpButtonBePushed || x.IsNumberButtonBePushed);

            return clicked;
        }

        public static Floor GetNextHigherBePushedFloor(this IElevatorState elevatorState)
        {
            var floor = elevatorState.ButtonStateInFloors
                .Where(x => x.Floor.Number > elevatorState.CurrentFloor.Number)
                .First();

            return floor.Floor;
        }

        public static bool IsLowerFloorButtonBePushed(this IElevatorState elevatorState)
        {
            // ReSharper disable once ComplexConditionExpression
            var clicked = elevatorState.ButtonStateInFloors
                .Where(x => x.Floor.Number < elevatorState.CurrentFloor.Number)
                .Any(x => x.IsDownButtonBePushed || x.IsNumberButtonBePushed);

            return clicked;
        }

        public static Floor GetNextLowerBePushedFloor(this IElevatorState elevatorState)
        {
            var floor = elevatorState.ButtonStateInFloors
                .OrderByDescending(x=>x.Floor.Number)
                .Where(x => x.Floor.Number < elevatorState.CurrentFloor.Number)
                .First();

            return floor.Floor;
        }

        public static void ResetButtonsWhenElevatorStillLifting(this IElevatorState elevatorState)
        {
            var buttonState = elevatorState.ButtonStateInFloors.Single(x => Equals(x.Floor, elevatorState.CurrentFloor));

            buttonState.ResetUpwardButtons();
        }

        public static void ResetButtonsWhenElevatorStillDecling(this IElevatorState elevatorState)
        {
            var buttonState = elevatorState.ButtonStateInFloors.Single(x => Equals(x.Floor, elevatorState.CurrentFloor));

            buttonState.ResetDownwardButtons();
        }

        public static void ResetButtonsWhenElevatorIsStopped(this IElevatorState elevatorState)
        {
            var buttonState = elevatorState.ButtonStateInFloors.Single(x => Equals(x.Floor, elevatorState.CurrentFloor));

            buttonState.ResetAllButtons();
        }
    }
}
