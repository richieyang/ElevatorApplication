﻿using System;

namespace ElevatorApplication.Elevator
{
    public class Floor
    {
        public int Number { get; private set; }

        private Floor(int number)
        {
            Number = number;
        }

        static Floor()
        {
            FloorHeight=new Floor(20);
        }

        public Floor GetNextHigherFloor()
        {
            return Parse(Number + 1);
        }

        public Floor GetNextLowerFloor()
        {
            return Parse(Number - 1);
        }

        public override bool Equals(object obj)
        {
            var floor = obj as Floor;
            if (floor == null)
            {
                return false;
            }
            return floor.Number == Number;
        }

        public override int GetHashCode()
        {
            return Number.GetHashCode();
        }

        public override string ToString()
        {
            return "Current Floor is " + Number;    
        }

        public static Floor FloorHeight { get; set; }

        public static Floor FirstFloor { get { return Parse(1); } }

        public static Floor Parse(int number)
        {
            if (number < 1 || number > FloorHeight.Number)
                throw new ArgumentException("Floor number " + number + " is between 1-" + FloorHeight.Number);

            return new Floor(number);
        }
    }
}