﻿using System;
using ElevatorApplication.Elevator;

namespace ElevatorApplication.Fluent
{
    public static class Elevator
    {
        public static IElevator Config(Action<ElevatorConfig> configAction)
        {
            var config=new ElevatorConfig();
            configAction(config);

            var elevator = new ElevatorApplication.Elevator.Elevator(config);
            return elevator;
        }

        public static IElevator PushUpButtonIn(this IElevator elevator, Floor floor)
        {
            elevator.PushUpButton(floor);

            return elevator;
        }

        public static IElevator PushDownButtonIn(this IElevator elevator, Floor floor)
        {
            elevator.PushDownButton(floor);

            return elevator;
        }

        public static IElevator PushNumberButtonIn(this IElevator elevator, Floor floor)
        {
            elevator.PushNumberButton(floor);

            return elevator;
        }

        public static IElevator Start(this IElevator elevator)
        {
            var runnable = elevator as ICanStart;
            runnable.Start();

            return elevator;
        }
    }
}