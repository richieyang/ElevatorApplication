﻿using System.Collections.Generic;

namespace ElevatorApplication
{
    public interface IElevator
    {
        int CurrentFloorNumber { get; }

        int FloorHeight { get; }

        void ChangeSate(IElevatorState newState);

        void SetCurrentFloorNumber(int floorNumber);

        List<ButtonsOfFloor> ButtonStateInFloors { get; }

        void PushUpButton(int floorNumber);

        void PushDownButton(int floorNumber);

        void PubshNumberButton(int floorNumber);

        void Start();
    }
}