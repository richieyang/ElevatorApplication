﻿using ElevatorApplication.Elevator;
using ElevatorApplication.Fluent;
using FluentAssert;
using Xunit;
using Xunit.Sdk;

namespace ElevatorApplication.Tests
{
    [Collection("elevator")]
    public class WhenPushedLowerFloorTests
    {
        [Fact]
        public void After_pushedSecondFloorButton_should_arrivedSecondFloor()
        {
            var elevator = Fluent.Elevator.Config(config => { config.InitializedFloor = Floor.Parse(3); })
                .PushDownButtonIn(Floor.Parse(2))
                .Start();

            elevator.CurrentFloor.ShouldBeEqualTo(Floor.Parse(2));
        }

        [Fact]
        public void After_pushedSomeDownButtons_should_arrivedDestinationFloor()
        {
            var elevator = Fluent.Elevator.Config(c => { c.InitializedFloor = Floor.Parse(9); })
                .PushDownButtonIn(Floor.Parse(3))
                .PushDownButtonIn(Floor.Parse(5))
                .PushDownButtonIn(Floor.Parse(8))
                .PushNumberButtonIn(Floor.Parse(9))
                .Start();

            elevator.CurrentFloor.Number.ShouldBeEqualTo(3);
        }
    }
}