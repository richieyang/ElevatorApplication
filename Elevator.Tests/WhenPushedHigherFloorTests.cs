﻿using ElevatorApplication.Elevator;
using ElevatorApplication.Fluent;
using FluentAssert;
using Xunit;

namespace ElevatorApplication.Tests
{
    [Collection("elevator")]
    public class WhenPushedHigherFloorTests
    {
        [Fact]
        public void After_pushedSecondFloorButton_should_arrivedSecondFloor()
        {
            var elevator = Fluent.Elevator.Config(config => { })
                .PushUpButtonIn(Floor.Parse(2))
                .Start();

            elevator.CurrentFloor.Number.ShouldBeEqualTo(2);
        }

        [Fact]
        public void After_pushedSomeUpButtons_should_arrivedDestinationFloor()
        {
            var elevator = Fluent.Elevator.Config(c => { c.InitializedFloor = Floor.Parse(2); })
                .PushUpButtonIn(Floor.Parse(3))
                .PushUpButtonIn(Floor.Parse(5))
                .PushUpButtonIn(Floor.Parse(8))
                .PushNumberButtonIn(Floor.Parse(10))
                .Start();

            elevator.CurrentFloor.Number.ShouldBeEqualTo(10);
        }
    }
}