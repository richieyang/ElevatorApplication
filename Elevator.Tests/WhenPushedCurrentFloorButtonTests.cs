﻿using ElevatorApplication.Elevator;
using ElevatorApplication.Fluent;
using FluentAssert;
using Xunit;

namespace ElevatorApplication.Tests
{
    [Collection("elevator")]
    public class WhenPushedCurrentFloorButtonTests
    {
        [Fact]
        public void When_noButtonBePushed_should_nothingHappen()
        {
            var elevator = Fluent.Elevator.Config(c => { c.InitializedFloor = Floor.FirstFloor; })
                .Start();

            elevator.CurrentFloor.Number.ShouldBeEqualTo(1);
        }

        [Fact]
        public void When_pushedUpButton_should_nothingHappen()
        {
            var elevator = Fluent.Elevator.Config(c => c.InitializedFloor = Floor.Parse(2))
                .PushUpButtonIn(Floor.Parse(2))
                .Start();

            elevator.CurrentFloor.Number.ShouldBeEqualTo(2);
        }
    }
}