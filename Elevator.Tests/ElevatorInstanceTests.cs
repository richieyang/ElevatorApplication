﻿using System.Linq;
using ElevatorApplication.Elevator;
using FluentAssert;
using Xunit;

namespace ElevatorApplication.Tests
{
    [Collection("elevator")]
    public class ElevatorInstanceTests
    {
         private readonly IElevator _elevator;

         public ElevatorInstanceTests()
         {
             _elevator = Fluent.Elevator.Config(config => { });
         }

        [Fact]
        public void CurrentFloorNumber_should_be_1()
        {
            _elevator.CurrentFloor.Number.ShouldBeEqualTo(Floor.FirstFloor.Number);
        }

        [Fact]
        public void ButtonStateInFloors_should_be_initialized()
        {
            var elevator = (Elevator.Elevator) _elevator;

            elevator.ButtonStateInFloors.All(x => !x.IsDownButtonBePushed);
            elevator.ButtonStateInFloors.All(x => !x.IsUpButtonBePushed);
            elevator.ButtonStateInFloors.All(x => !x.IsNumberButtonBePushed);
        }
    }
}
